# GitLab Auth helper for React

```js
import { GitlabAuth, useGitlab } from "gitlab-auth";

function App() {
  return (
    <GitlabAuth
      host="https://gitlab.example.com/"
      application_id="xxxx(Application ID setup in your gitlab instance as admin under applications)xxx"
      redirect_uri="http://localhost:3000/auth/"
      scope="api openid profile email"
    >
      <>This is rendered with GitLab access!</>
    </GitlabAuth>
  );
}
```

To get access to the gitlab API and list branches:

```js
import { useState, useEffect } from "react";
import { useGitlab } from "gitlab-auth";

function BranchesList() {
  const [branches, setBranches] = useState(null);
  const gitlab = useGitlab();
  useEffect(() => {
    gitlab.Branches.all("myprojectname").then((b) => {
      setBranches(b);
    });
  }, [gitlab]);
  return (
    <>
      {branches &&
        branches.map((branch, idx) => {
          <div key={idx}>{branch.name}</div>;
        })}
    </>
  );
}
```

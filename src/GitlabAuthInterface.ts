export interface GitlabAuthProps {
  /**
   * GitLab Host URI (eg. "https::/gitlab.example.com")
   */
  host: string;
  /**
   * GitLab Application ID (from your gitlab instance under Admin Area / Applications)
   */
  application_id: string;
  /**
   * GitLab Application Secret (from your gitlab instance under Admin Area / Applications)
   */
  secret: string;
  /**
   * GitLab RedirectURI the redirect URI to used (needs to be specified in the gitlab Admin Area / Application)
   */
  redirect_uri: string;
  /**
   * Scopes you want access to, default = "openid profiel email" use GitLab named scopes for more access
   */
  scope: string;
  /**
   * Public URL of the application (used for redirecting to the application after login)
   */
  public_url: string;
}

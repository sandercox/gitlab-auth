import React, { FC, useMemo } from "react";
import { AuthProvider, UserManager, useAuth } from "oidc-react";
import { Gitlab } from "@gitbeaker/rest";

import { GitlabAuthContext } from "./GitlabAuthContext";
import { GitlabAuthProps } from "./GitlabAuthInterface";

// Remove oauth info from Querystring
const cleanup = (uri: string | null) => {
  window.history.replaceState(
    {},
    window.document.title,
    uri ? uri : window.location.href.replace(/\?.*/, "")
  );
  if (uri != null) {
    window.location.replace(uri);
  }
};

interface GitlabAuthContextMakerProps {
  host: string;
  public_url: string;
}

// Gitlab Auth context with gitlab api setup
const GitlabAuthContextMaker: FC<React.PropsWithChildren<GitlabAuthContextMakerProps>> = ({ public_url, host, children }) => {
  const auth = useAuth();
  const gitlabApi = useMemo(() => {
    if (auth == null || auth === undefined || auth.userData == null) return null;
    return new Gitlab({
      host: host,
      oauthToken: auth.userData!.access_token,
    });

  }, [auth.userData, host]);
  if (auth == null || auth === undefined) return <>No auth information</>;
  if (auth != null && auth.userData == null) {
    return (
      <>
        Waiting for userData / token information... (
        <a
          href={public_url + "/logout"}
          onClick={() => auth.signOut()}
          title="cancel sign-in"
        >
          Cancel
        </a>
        )
      </>
    );
  }

  return (
    <>
      <GitlabAuthContext.Provider value={gitlabApi}>
        {children}
      </GitlabAuthContext.Provider>
    </>
  );
};

export const GitlabAuth: FC<React.PropsWithChildren<GitlabAuthProps>> = ({
  host,
  application_id,
  redirect_uri,
  public_url,
  secret,
  scope = "openid profile email",
  children,
  ...props
}) => {
  // setup oauth usermanager to work with gitlab
  const userManager = new UserManager({
    authority: host,
    client_id: application_id,
    redirect_uri: redirect_uri,
    client_secret: secret,
    scope: scope,
    automaticSilentRenew: true,
    response_type: "code",
    loadUserInfo: false,
    metadata: {
      issuer: host,
      authorization_endpoint: host + "/oauth/authorize",
      token_endpoint: host + "/oauth/token",
      revocation_endpoint: host + "/oauth/revoke",
      introspection_endpoint: host + "/oauth/introspect",
      userinfo_endpoint: host + "/oauth/userinfo",
      jwks_uri: host + "/oauth/discovery/keys",
    },
  });
  return (
    <AuthProvider
      userManager={userManager}
      onBeforeSignIn={() => {
        localStorage.setItem("gitlab-auth-preauthuri", window.location.href);
        return "";
      }}
      onSignIn={() => {
        cleanup(localStorage.getItem("gitlab-auth-preauthuri"));
        localStorage.removeItem("gitlab-auth-preauthuri");
      }}
    >
      <GitlabAuthContextMaker host={host} public_url={public_url}>{children}</GitlabAuthContextMaker>
    </AuthProvider>
  );
};

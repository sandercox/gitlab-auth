import React from "react";
import { Gitlab } from "@gitbeaker/rest";

export const GitlabAuthContext = React.createContext<InstanceType<
  typeof Gitlab
> | null>(null);

import { useContext } from "react";
import { GitlabAuthContext } from "./GitlabAuthContext";
import { Gitlab } from "@gitbeaker/core";

export const useGitlab = () => {
  return useContext<InstanceType<typeof Gitlab> | null>(GitlabAuthContext);
};
